const { globalStats, MeasureUnit, AggregationType } = require('@opencensus/core');
const { PrometheusStatsExporter } = require('@opencensus/exporter-prometheus');

// 1. to create measure
const latency = globalStats.createMeasureDouble(
  'latency',// name
  MeasureUnit.MS,// measure init
  'The latency in milliseconds'// description
);
// 2. to create view, a view coupled a measure with aggregators (e.g. AggregationType.LastValue)
const latencyView = globalStats.createView(
  'myapp/latency',// name
  latency,// data
  AggregationType.LAST_VALUE,// aggregator
  [{ name: 'performance' }],// tags
  'The last latency',// description
);
// 3. to register view
globalStats.registerView(latencyView);
// 4. to create an instance of prometheus exporter
const exporter = new PrometheusStatsExporter({
  port: 9464,// metrics will be exported on https://localhost:{port}/metrics
  startServer: true
});
// 5. to pass the created exporter to global Stats
globalStats.registerExporter(exporter);

const recordData = () => {
  setInterval(() => {
    const currentLatency = Math.floor(Math.random() * (100 - 1)) + 1;

    const measurement = {
      measure: latency,
      value: currentLatency
    };

    console.log(`>>>>> latency recorded: ${currentLatency}`);
    // 6. to save measurement
    globalStats.record([measurement]);
  }, 5000)
}

recordData();